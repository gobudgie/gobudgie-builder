#!/bin/bash
set -e

unprepare ()
{
    sudo umount -R buildsystem/proc buildsystem/sys buildsystem/etc/resolv.conf || true
}

prepare ()
{
    # sudo mount -B /dev buildsystem/dev
    # sudo mount -B /dev/pts buildsystem/dev/pts
    sudo mount -t proc proc buildsystem/proc
    sudo mount -t sysfs sys buildsystem/sys
    # sudo mount -B /run buildsystem/run
    sudo mount -B /etc/resolv.conf buildsystem/etc/resolv.conf
}

bootstrap ()
{
    sudo rm -fr buildsystem

    #Computing current codename
    SUITE=$(curl http://archive.ubuntu.com/ubuntu/dists/devel/Release | grep -P "^Suite: ")
    SUITE=${SUITE##*: }

    sudo debootstrap ${SUITE} buildsystem
    sudo sh -c 'echo "deb https://archive.gobudgie.ml/ gobudgie main" > buildsystem/etc/apt/sources.list'
    sudo dpkg -x releases/all/gobudgie-keyring*.deb buildsystem/
    sudo chroot buildsystem dpkg --add-architecture i386
}

install-debs ()
{
    # setting dpkg configuration 
    sudo chroot buildsystem bash -c 'echo "refind  refind/install_to_esp   boolean false \n ttf-mscorefonts-installer       msttcorefonts/accepted-mscorefonts-eula boolean true \n grub-pc grub-pc/install_devices_empty   boolean false \n grub-pc grub-pc/install_devices multiselect \n " | sudo debconf-set-selections'
    
    sudo cp releases/all/*.deb buildsystem/
    sudo chroot buildsystem apt update -qq
    sudo chroot buildsystem apt dist-upgrade -y -qq
    sudo chroot buildsystem bash -c "apt install /*gobudgie*.deb -y --allow-downgrades"
    # Marking kernel as manually installed
    sudo chroot buildsystem apt install linux-generic
    sudo chroot buildsystem apt autoremove -y -qq
    sudo chroot buildsystem apt autoclean -y -qq
    sudo rm buildsystem/*.deb
    sudo chroot buildsystem update-initramfs -k all -c
}

unprepare
if [ ! -f buildsystem/bin/bash ]; then
    echo "No buildsystem found. Creating..."
    bootstrap
else
    # Checking if manually marked as update or rebuild
    if [[ ! "-" = "" && "$@" = *"rebuild"* ]]
    then
        echo "Creating buildsystem..."
        bootstrap
    elif [[ ! "-" = "" && "$@" = *"update"* ]]
    then
        echo "Updating buildsystem..."
    else
        read -p "Buildsystem found. Do you want to upgrade (u) or rebuild (r)? " -n 1 -r
        echo    # (optional) move to a new line
        if [[ $REPLY =~ ^[Rr]$ ]]
        then
            bootstrap
        fi
    fi
fi
prepare
install-debs
unprepare
