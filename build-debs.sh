#!/bin/bash
set -e
for DIR in projects/*/ ; do
	if [ -d "${DIR}src/DEBIAN/" ]
	then
		VERSION=$(cat "${DIR}src/DEBIAN/control" | grep -P "^Version: ")
		VERSION=${VERSION##*: }
		ARCH=$(cat "${DIR}src/DEBIAN/control" | grep -P "^Architecture: ")
		ARCH=${ARCH##*: }
		PACKAGE=$(cat "${DIR}src/DEBIAN/control" | grep -P "^Package: ")
		PACKAGE=${PACKAGE##*: }
		echo -e "\n"
		echo -e "\e[34mBuilding\e[39m \e[32m${PACKAGE}_${VERSION}_${ARCH}\e[39m ..."
		echo -e "\e[34mCreating\e[39m folder \e[31m'releases/${PACKAGE}/${VERSION}'\e[39m"
		mkdir -p "releases/${PACKAGE}/${VERSION}"
		echo -e "\e[34mPacking\e[39m source code to \e[31m'releases/${PACKAGE}/${VERSION}/${PACKAGE}_${VERSION}_source.tar'\e[39m"
		tar -cf "releases/${PACKAGE}/${VERSION}/${PACKAGE}_${VERSION}_source.tar" "${DIR}src/"
		echo -e "\e[34mBuilding\e[39m \e[31m'releases/${PACKAGE}/${VERSION}/${PACKAGE}_${VERSION}_${ARCH}.deb'\e[39m from \e[31m'${DIR}src'\e[39m"
		dpkg-deb --build "${DIR}src" "releases/${PACKAGE}/${VERSION}/${PACKAGE}_${VERSION}_${ARCH}.deb"
		echo -e "\e[34mLinking\e[39m \e[31m'releases/${PACKAGE}/current'\e[39m to \e[31m'releases/${PACKAGE}/${VERSION}'\e[39m"
		unlink "releases/${PACKAGE}/current" || true
		ln -s "${VERSION}" "releases/${PACKAGE}/current"
		echo -e "\e[34mBuilding\e[39m Package \e[32m${PACKAGE}\e[39m succeed!\n"
	else
		echo -e "\e[31m${DIR}src/DEBIAN/\e[39m doesn't exist. Skipping..."
	fi
done
echo -e "\n\e[34mCopying\e[39m all packages\e[32m to \e[31mreleases/all/\e[39m"
rm -rf releases/all/*
cp releases/*/current/*.deb releases/all/
echo -e "\e[34mFinished.\e[39m"
