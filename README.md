# Gobudgie Builder

Build Gobudgie Linux from scratch

```
make world
```

## Install dependencies

### Debian/Ubuntu
```
sudo ./install-dependencies.sh
```
**OR**
```
sudo apt install git genisoimage squashfs-tools tar dpkg debootstrap syslinux-utils
```

### Arch Linux/Manjaro
```
sudo pacman -S debootstrap cdrtools git squashfs-tools util-linux
sudo yay -S dpkg
```