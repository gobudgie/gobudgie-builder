echo "Creating live filesystem and packing kernel..."

sudo rm isotemp/casper/*
sudo rm -r buildsystem/tmp/*
sudo mksquashfs buildsystem/ isotemp/casper/filesystem.squashfs -e vmlinuz.old initrd.img.old
sudo cp buildsystem/boot/vmlinuz isotemp/casper/vmlinuz
sudo cp buildsystem/boot/initrd.img isotemp/casper/initrd

echo "Creating manifest..."
sudo chroot buildsystem/ dpkg-query -W --showformat='${Package} ${Version}\n' | sudo tee isotemp/casper/filesystem.manifest
sudo cp -v isotemp/casper/filesystem.manifest isotemp/casper/filesystem.manifest-desktop
REMOVE='ubiquity ubiquity-frontend-gtk ubiquity-frontend-kde casper lupin-casper casper live-initramfs user-setup discover1 xresprobe os-prober libdebian-installer4 gobudgie-live gparted ubiquity-slideshow-gobudgie'
for i in $REMOVE
do
        sudo sed -i "/${i}/d" isotemp/casper/filesystem.manifest-desktop
done
printf $(sudo du -sx --block-size=1 isotemp | cut -f1) > isotemp/casper/filesystem.size

echo "Create MD5 sums..."
sudo sh -c '(cd isotemp && find . -type f -print0 | xargs -0 md5sum | grep -v "\./md5sum.txt" > md5sum.txt)'

timestamp=$(date +%Y-%m-%d.%H%m)

sudo genisoimage -cache-inodes -r -J -l -V "Gobudgie rolling" -b isolinux/isolinux.bin -c isolinux/boot.cat \
        -no-emul-boot -boot-load-size 4 -allow-limited-size -boot-info-table \
	-o dist/gobudgie-rolling-${timestamp}.iso isotemp/
sudo isohybrid dist/gobudgie-rolling-${timestamp}.iso

sudo unlink dist/gobudgie-rolling-current.iso || true
sudo ln dist/gobudgie-rolling-${timestamp}.iso dist/gobudgie-rolling-current.iso
