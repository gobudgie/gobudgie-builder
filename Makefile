job="update"

debs:
	./build-debs.sh
fetch:
	git pull
	./fetch.py
system:
	./build-system.sh $(job)
iso:
	./build-iso.sh
world:
	git pull
	./fetch.py
	./build-debs.sh
	./build-system.sh $(job)
	./build-iso.sh
