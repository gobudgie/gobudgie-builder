#!/usr/bin/python3
import json
import os
import urllib.request
# Creating "blacklist" of projects
blacklist = ["gobudgie-builder","anbox","gobudgie-user-agent-chrome"]
# Fetching project list from remote server
with urllib.request.urlopen('https://gitlab.com/api/v4/groups/gobudgie/projects') as response:
   projects = json.loads(response.read())
# Clearing gitignore file
for pro in projects:
    if pro["path"] in blacklist:
        print("Project '"+pro["path"]+"' is blacklisted!")
    else:
        # Adding current project to .gitignore
        # If project dir already exists update code, otherwhise clone
        if(os.path.exists("projects/"+pro["path"])):
            print("Pulling latest version of "+pro["name_with_namespace"]+"...")
            os.system("cd projects/"+pro["path"]+" && git pull && cd ../..")
        else:
            print("Cloning "+pro["name_with_namespace"]+" via ssh...")
            os.system("git clone "+pro["http_url_to_repo"]+" projects/"+pro["path"])
print("Finished.")
